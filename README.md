## curd 生成器

### 安装

#### 指定代码源并安装

```php

"hauser/framework": {
      "type": "git",
      "url": "git@gitee.com:hauser/laravel-generator.git"
 }
    
 composer require  hauser/framework   
    
```

### 配置

```php
打开 app/Console/Kernel.php

use Laravel\ModulesGenerator\Module;

protected $commands = [
        //
        Module::class,
];

```

### 使用

```bash
php artisan make:module
按照提示，生成就行。支持多级目录。
比如第一步设置表名时，可以传入 test/activity。
就会在Test目录下生成ActivityController.php 等文件。
       
```