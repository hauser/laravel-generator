<?php
namespace App\Models\MySQL{namespace};

/**
 * Class {$className}
 * {$tableComment}
 * @package App\Models
 * @author  {$author}
 * @since   {$currentDate}
 */

use App\Library\Framework\Helper;
use App\Models\MySQL\BaseModel;

class {$className} extends BaseModel
{
    /**
     * 表名
     */
    protected $table = '{$table}';

    /**
     * 缓存组名
     */
    static protected $cacheTag = '{$table}';

    /**
     * 可入库字段
     */
    protected $fillAble = {$fields};

    //disable incrementing id;
    public $incrementing = FALSE;

    protected $guarded = [];
    /**
     * 隐藏输出字段
     */
    protected $hidden = [];

    /**
     * 数据库连接
     *
     * @var string
     */
    protected $connection = 'mysql';

    public function getFillAble()
    {
        return $this->fillAble;
    }

    //Model RelationShip

    /**
     * 聚集查询
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilter($query, $params)
    {
        {$scopeFilterString}

        return $query;
    }

    /**
    * @title   分页及导出查询
    * @desc    分页及导出数据查询
    * @version 1.0.0
    * @level 1
    * @author {$author}
    * @package App\Models\MySQL
    * @since {$currentDate}
    * @params  type filedName required?
    * @param array $params
    * @return mixed
    * @returns
    * []
    * @returns
    */
    static public function getList(array $params)
    {
        $pageSize = isset($params['per_page']) && $params['per_page'] ? intval($params['per_page']) : 10;
        $page = isset($params['page']) ? intval($params['page']) : 1;


        $cacheName = __METHOD__ . var_export($params, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);

        if (!$data) {
            if (isset($params['sortField']) && isset($params['sort'])) {
                $orderField = $params['sortField'];
                $orderType  = $params['sort'];
            } else {
                $orderField = 'created_at';
                $orderType  = 'desc';
            }

            $sqlObj = self::Filter($params)->orderBy($orderField, $orderType);

            if (isset($params['_export']) && $params['_export'] == 1) {
                $data = $sqlObj->get();
            } else {
                $data = $sqlObj->Paginate($pageSize, ['*'], 'page', $page);
            }
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }

    /**
     * @title   根据id查询数据
     * @desc    根据id查询数据
     * @version 1.0.0
     * @author {$author}
     * @package App\Models\MySQL
     * @param id
     * @params  char id 主键  是
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    static public function getById($id = NULL)
    {
        $cacheName = __METHOD__ . var_export($id, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);
        if (!$data) {
            $data = self::where('id', $id)->first();
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }

    /**
     * @title   根据id数组，查找多条
     * @desc
     * @version 1.0.0
     * @author {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $idList
     * @return mixed
     * @returns
     * []
     * @returns
     */
    static public function getByIdList(array $idList = [])
    {
        $cacheName = __METHOD__ . var_export($idList, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);
        if (!$data) {
            $data = self::whereIn("id", $idList)->get();
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }


    /**
     * @title   添加数据
     * @desc
     * @version  1.0.0
     * @author  {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $params
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    static public function add(array $params)
    {
        $params['id'] = Helper::uuid();
        $data         = self::create($params);
        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }

    /**
     * @title   修改记录
     * @desc
     * @version  1.0.0
     * @author  {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $params
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    static public function edit(array $params)
    {
        $record = self::where('id', $params['id'])->first();
        if (!$record) {
            throw new \RuntimeException("该数据不存在", 2);
        }

        $data = $record->update($params);
        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }


    /**
    * @title   删除记录
    * @desc
    * @version  1.0.0
    * @author  yantao
    * @package App\Models\MySQL
    * @since
    * @params  type filedName required?
    * @param array $id
    * @return mixed
    * @returns
    * int
    * @returns
    */
    static public function destroyById($id)
    {
        $data = self::where('id', $id)->delete();
        app('cache')->tags(self::$cacheTag)->flush();
        return $data;
    }
}