<?php
namespace App\Models\MySQL;

/**
 * Class {$className}
 * {$tableComment}
 * @package App\Models
 * @author  {$author}
 * @since   {$currentDate}
 */

use App\Library\Framework\Helper;

class {$className} extends BaseModel
{
    /**
     * 表名
     */
    protected $table = '{$table}';

    /**
     * 缓存组名
     */
    static protected $cacheTag = '{$table}';

    /**
     * 可入库字段
     */
    protected $fillAble = {$fields};

    //disable incrementing id;
    public $incrementing = FALSE;

    protected $guarded = [];
    /**
     * 隐藏输出字段
     */
    protected $hidden = [];

    /**
     * 数据库连接
     *
     * @var string
     */
    protected $connection = 'mysql';

    public function getFillAble()
    {
        return $this->fillAble;
    }

    //Model RelationShip

    /**
     * 聚集查询
     * @param $query
     * @param $params
     * @return mixed
     */
    public function scopeFilter($query, $params)
    {
        {$scopeFilterString}

        return $query;
    }

    /**
    * @title   分页及导出查询
    * @desc    分页及导出数据查询
    * @version 1.0.0
    * @level 1
    * @author {$author}
    * @package App\Models\MySQL
    * @since {$currentDate}
    * @params  type filedName required?
    * @param array $params
    * @return mixed
    * @returns
    * []
    * @returns
    */
    static public function getList(array $params)
    {
        $pageSize = isset($params['per_page']) && $params['per_page'] ? intval($params['per_page']) : 10;
        $page = isset($params['page']) ? intval($params['page']) : 1;


        $cacheName = __METHOD__ . var_export($params, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);

        if (!$data) {
            if (isset($params['sortField']) && isset($params['sort'])) {
                $orderField = $params['sortField'];
                $orderType  = $params['sort'];
            } else {
                $orderField = 'createtime';
                $orderType  = 'desc';
            }

            $sqlObj = self::Filter($params)->orderBy($orderField, $orderType);

            if (isset($params['_export']) && $params['_export'] == 1) {
                $data = $sqlObj->get();
            } else {
                $data = $sqlObj->Paginate($pageSize, ['*'], 'page', $page);
            }
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }

    /**
     * @title   根据id查询数据
     * @desc    根据id查询数据
     * @version 1.0.0
     * @author {$author}
     * @package App\Models\MySQL
     * @param id
     * @params  char id 主键  是
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    static public function getById($id = NULL)
    {
        $cacheName = __METHOD__ . var_export($id, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);
        if (!$data) {
            $data = self::where('id', $id)->first();
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }

    /**
     * @title   根据id数组，查找多条
     * @desc
     * @version 1.0.0
     * @author {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $idList
     * @return mixed
     * @returns
     * []
     * @returns
     */
    static public function getByIdList(array $idList = [])
    {
        $cacheName = __METHOD__ . var_export($idList, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);
        if (!$data) {
            $data = self::whereIn("id", $idList)->get();
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }

    /**
    * @title   根据third_id，查找多条
    * @desc
    * @version 1.0.0
    * @level 1
    * @author {$author}
    * @package App\Models\MySQL
    * @since {$currentDate}
    * @params  type filedName required?
    * @param array $params
    * @return mixed
    * @returns
    * []
    * @returns
    */
    static public function getByThirdIdList(array $params = [])
    {
        $cacheName = __METHOD__ . var_export($params, TRUE);
        $data      = app('cache')->tags(self::$cacheTag)->get($cacheName);
        if (!$data) {
            $data = self::Filter($params)->get();
            app('cache')->tags(self::$cacheTag)->put($cacheName, $data, Config('cache.expireTime'));
        }

        return $data;
    }

    /**
     * @title   添加数据
     * @desc
     * @version  1.0.0
     * @author  {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $params
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    static public function add(array $params)
    {
        $params['id'] = Helper::uuid();
        $data         = self::create($params);
        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }

    /**
     * @title   修改记录
     * @desc
     * @version  1.0.0
     * @author  {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $params
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    static public function edit(array $params)
    {
        $record = self::where('id', $params['id'])->first();
        if (!$record) {
            throw new \RuntimeException("该数据不存在", 2);
        }

        $data = $record->update($params);
        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }

    /**
    * @title    修改记录
    * @desc
    * @version  1.0.0
    * @level    1
    * @author  {$author}
    * @package App\Models\MySQL
    * @since {$currentDate}
    * @params   type filedName required?
    * @param array $params
    * @return mixed
    * @returns
    * {}
    * @returns
    */
    static public function updateByThirdIdAndSysId(array $params)
    {
        $record = self::where('third_id', $params['third_id'])
        ->where('sys_id', $params['sys_id'])
        ->first();

        $params['updated_at'] = date("Y-m-d H:i:s");
        if (!$record) {
            $data = self::add($params);
        } else {
            $data = $record->update($params);
        }

        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }

    /**
     * @title   删除记录
     * @desc
     * @version  1.0.0
     * @author  {$author}
     * @package App\Models\MySQL
     * @since
     * @params  type filedName required?
     * @param array $params
     * @return mixed
     * @returns
     * int
     * @returns
     */
    static public function remove(array $params = [])
    {
        $data = self::whereIn('id', $params['idList'])->delete();
        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }

    /**
    * @title    删除记录根据third_id删除
    * @desc
    * @version  1.0.0
    * @level    1
    * @author  {$author}
    * @package App\Models\MySQL
    * @since {$currentDate}
    * @params   type filedName required?
    * @param array $params
    * @return mixed
    * @returns
    * int
    * @returns
    */
    static public function removeByThirdIdAndSysId(array $params = [])
    {
        if (!isset($params['thirdIdList']) || !is_array($params['thirdIdList'])) {
            throw new \RuntimeException('thirdIdList must exist and it\'s array', 2);
        }

        $data = self::whereIn('third_id', Helper::arrayValueToString($params['thirdIdList']))
        ->where('sys_id', $params['sys_id'])
        ->delete();

        app('cache')->tags(self::$cacheTag)->flush();

        return $data;
    }
}