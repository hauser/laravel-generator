<?php
/**
 * Created by PhpStorm.
 * @author {$author}
 * @since {$currentDate}
 */

namespace App\Library\Validators{namespace};

class {$className}Validator
{
    static public function store($params)
    {
        $validation =  [
            'rules'   => [
                'third_id'           => 'required',
                'sys_id'       => 'required|exists:mysql.system,id',
            ],
            'message' => [
                'third_id.required'           => 'id不能为空',
                'sys_id.required'       => '系统ID不能为空',
                'sys_id.exists'         => '系统ID不存在',
            ]
        ];

        return $validation;
    }

    static public function update($params)
    {
        $validation =  [
            'rules'   => [
                'third_id'           => 'required',
                'sys_id'       => 'required|exists:mysql.system,id',
            ],
            'message' => [
                'third_id.required'           => 'id不能为空',
                'sys_id.required'       => '系统ID不能为空',
                'sys_id.exists'         => '系统ID不存在',
            ]
        ];

        return $validation;
    }

    static public function destroy($params)
    {
        $validation =  [
            'rules'   => [
                'id'           => 'required',
                'sys_id'       => 'required|exists:mysql.system,id',
            ],
            'message' => [
                'id.required'           => 'id不能为空',
                'sys_id.required'       => '系统ID不能为空',
                'sys_id.exists'         => '系统ID不存在',
            ]
        ];

        return $validation;
    }

    static public function getList()
    {
        return [
                'rules'   => [
                'orgcode' => 'required'
            ],
            'message' => [
                'orgcode.required' => 'orgcode不能为空',
            ]
        ];
    }
}