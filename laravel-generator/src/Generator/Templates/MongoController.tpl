<?php
namespace App\Http\Controllers;

/**
 * Class {$controllerName}
 * @package App\Http\Controllers
 * @author {$author}
 * @since {$currentDate}
 */

use Illuminate\Http\Request;
use App\Models\Mongo\{$className};
use App\Library\Framework\Validate;
use App\Library\Framework\Helper;
use Gos\Validators\{$className}Validator;

class {$controllerName} extends Controller
{
    /**
     * @title   列表查询
     * @desc    列表分页查询
     * @version 1.0
     * @level 1
     * @author  {$author}
     * @package App\Http\Controllers\
     * @since   {$currentDate}
     * @params
     * @param Request $request
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    public function index(Request $request)
    {
        $params = $request->all();
        //校验
        Validate::check($params, {$className}Validator::getList());

        $data = {$className}::getList($params);
        /*
            if ($request->has('_export') && $request->get('_export') == 1) {
                \Excel::create('CardViceApp', function ($excel) use ($data) {
                    $excel->sheet('CardViceApp', function ($sheet) use ($data) {
                        $sheet->fromArray($data->toArray());
                    });
                })->export('xlsx');
            }
        */

        return Helper::responseJson($data);
    }


    /**
    * @title   批量添加
    * @desc    批量添加
    * @version 1.0
    * @level 1
    * @author  {$author}
    * @package App\Http\Controllers\
    * @since   {$currentDate}
    * @params
    * @param Request $request
    * @return mixed
    * @returns
    * {}
    * @returns
    */
    public function batchStore(Request $request)
    {
        $params = $request->all();
        if (!isset($params['data']) || !$params['data']) {
            throw new \RuntimeException('data不存在或为空', 2);
        }

        $data = [
            'errorData'    => [],
            'errorMessage' => [],
            'successData'  => [],
        ];

        try {
            foreach ($params['data'] as $k => $v) {
                $v['third_id'] = isset($v['id']) ? $v['id'] : null;
                $v['sys_id'] = $params['sys_id'];
                unset($v['id']);

                try {
                    //校验
                    Validate::check($v, {$className}Validator::store($params));

                    {$className}::add($v);
                    $data['successData'][] = $v['third_id'];
                } catch (\Exception $e) {
                    $data['errorData'][] = $v;
                    $data['errorMessage'][ $v['third_id'] ] = $e->getMessage();
                    \Log::error($e->getMessage(), ['exception'=>strval($e)]);

                    continue;
                }
            }
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode() > 1 ? $e->getCode() : 2);
        }

        return Helper::responseJson($data);
    }

    /**
     * @title   批量编辑
     * @desc    批量编辑
     * @version 1.0
     * @level 1
     * @author  {$author}
     * @package App\Http\Controllers\
     * @since   {$currentDate}
     * @params
     * @param Request $request
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    public function batchUpdateByThirdIdAndSysId(Request $request)
    {
        $params = $request->all();
        if (!isset($params['data']) || !$params['data']) {
            throw new \RuntimeException('data不存在或为空', 2);
        }

        $data = [
            'errorData'    => [],
            'errorMessage' => [],
            'successData'  => [],
        ];

        try {
            foreach ($params['data'] as $k => $v) {
                $v['third_id'] = isset($v['id']) ? $v['id'] : null;
                $v['sys_id'] = $params['sys_id'];
                unset($v['id']);

                try {
                    //校验
                    Validate::check($v, {$className}Validator::update($params));

                    {$className}::updateByThirdIdAndSysId($v);
                    $data['successData'][] = $v['third_id'];
                } catch (\Exception $e) {
                    $data['errorData'][] = $v;
                    $data['errorMessage'][ $v['third_id'] ] = $e->getMessage();
                    \Log::error($e->getMessage(), ['exception'=>strval($e)]);

                    continue;
                }
            }
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode() > 1 ? $e->getCode() : 2);
        }

        return Helper::responseJson($data);
    }

    /**
     * @title   详情
     * @desc    详情
     * @version 1.0
     * @level 1
     * @author  {$author}
     * @package App\Http\Controllers\
     * @since   {$currentDate}
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     * @returns
     * []
     * @returns
     */
    public function show(Request $request)
    {
        $data = {$className}::getById($request->get('id'));

        return Helper::responseJson($data);
    }

    /**
    * @title   删除
    * @desc    删除
    * @version 1.0
    * @level 1
    * @author {$author}
    * @package App\Http\Controllers\
    * @since {$currentDate}
    * @params
    * @param Request $request
    * @return mixed
    * @returns
    * {}
    * @returns
    */
    public function destroyByThirdIdAndSysId(Request $request)
    {
        $params = $request->all();
        $params['third_id'] = isset($params['id']) ? $params['id'] : \NULL;
        unset($params['id']);

        //校验
        Validate::check($params, {$className}Validator::destroy($params));

        $data = [
            'errorData'    => [],
            'errorMessage' => [],
            'successData'  => [],
        ];

        $thirdIdList = !is_array($params['third_id']) ? array_map('strval', explode(",", $params['third_id'])) : $params['third_id'];
        try {
            $totalNum = {$className}::removeByThirdIdAndSysId(['thirdIdList' => $thirdIdList, 'sys_id' => $params['sys_id']]);
            $data['successData'] = $totalNum;
        } catch (\Exception $e) {
            $data['errorData'] = $thirdIdList;
            $data['errorMessage'] = $e->getMessage();
            \Log::error($e->getMessage(), ['exception'=>strval($e)]);

            //throw new \RuntimeException($e->getMessage(), $e->getCode() > 1 ? $e->getCode() : 2);
        }

        return Helper::responseJson($data);
    }

}