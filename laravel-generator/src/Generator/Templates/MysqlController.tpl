<?php
namespace App\Http\Controllers{namespace};

/**
 * Class {$controllerName}
 * @package App\Http\Controllers
 * @author {$author}
 * @since $currentDate
 */

use Illuminate\Http\Request;
use App\Models\MySQL{namespace}\{$className};
use App\Library\Framework\Validate;
use Library\Validators{namespace}\{$className}Validator;
use App\Library\Framework\Helper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class {$controllerName} extends Controller
{
    /**
     * @title   列表查询
     * @desc    列表分页查询
     * @version 1.0
     * @level 1
     * @author {$author}
     * @package App\Http\Controllers\
     * @since
     * @params
     * @param Request $request
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    public function index(Request $request)
    {
        $params = $request->all();
        //校验
        Validate::check($params, {$className}Validator::getList());
        $data = {$className}::getList($params);

        return Helper::responseJson($data);
    }

    /**
     * @title   添加
     * @desc    添加
     * @version 1.0
     * @level 1
     * @author {$author}
     * @package App\Http\Controllers\
     * @since
     * @params
     * @param Request $request
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    public function create(Request $request)
    {
        $params = $request->all();

        app('db')->connection('mysql')->beginTransaction();

        try {
            //校验
            Validate::check($params, {$className}Validator::store($params));

            $result = {$className}::add($params);
        } catch (\Exception $e) {
            $data =[];
            $data['errorData'][] = $params;
            $data['errorMessage'] = $e->getMessage();
            app('db')->connection('mysql')->rollback();
            Log::error($e->getMessage(), ['exception'=>strval($e)]);
            throw new \RuntimeException($e->getMessage(), $e->getCode() > 1 ? $e->getCode() : 2);
        }

        app('db')->connection('mysql')->commit();

        return Helper::responseJson($result);
    }

    /**
     * @title   编辑
     * @desc    编辑
     * @version 1.0
     * @level 1
     * @author {$author}
     * @package App\Http\Controllers\
     * @since
     * @params char id id 是
     * @param Request $request
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    public function update(Request $request)
    {
        $params = $request->all();

        app('db')->connection('mysql')->beginTransaction();

        try {
            //校验
            Validate::check($params, {$className}Validator::update($params));

            $result = {$className}::edit($params);
        } catch (\Exception $e) {
            $data = [];
            $data['errorData'] = $params;
            $data['errorMessage'] = $e->getMessage();
            Log::error($e->getMessage(), ['exception'=>strval($e)]);
            app('db')->connection('mysql')->rollback();
            throw new \RuntimeException($e->getMessage(), $e->getCode() > 1 ? $e->getCode() : 2);
        }

        app('db')->connection('mysql')->commit();

        return Helper::responseJson($result);
    }

    /**
     * @title   详情
     * @desc    详情
     * @version 1.0
     * @level 1
     * @author {$author}
     * @package App\Http\Controllers
     * @since
     * @params  char id ID 是
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @returns
     * []
     * @returns
     */
    public function show(Request $request)
    {
        $id = $request->get('id');
        $data = {$className}::getById($id);

        return Helper::responseJson($data);
    }

    /**
     * @title   删除
     * @desc    删除
     * @version 1.0
     * @level 1
     * @author {$author}
     * @package App\Http\Controllers\
     * @since
     * @params
     * @param Request $request
     * @return mixed
     * @returns
     * {}
     * @returns
     */
    public function destroy(Request $request)
    {
        $params = $request->all();
        //校验
        Validate::check($params, {$className}Validator::destroy($params));

        $id = $params['id'];

        app('db')->connection('mysql')->beginTransaction();
        try {
            $totalNum = {$className}::destroyById($id);
            app('db')->connection('mysql')->commit();
        } catch (\Exception $e) {
            $data = [];
            $data['errorData'] = $id;
            $data['errorMessage'] = $e->getMessage();
            Log::error($e->getMessage(), ['exception'=>strval($e)]);
            throw new \RuntimeException($e->getMessage(), $e->getCode() > 1 ? $e->getCode() : 2);
        }

        return Helper::responseJson($result);
    }

}