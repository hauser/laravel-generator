<?php

namespace Laravel\ModulesGenerator\Generator\Lib;

/**
 * Created by PhpStorm.
 * User: hauser
 * Date: 17-3-28
 * Time: 下午5:54
 */
class Helper
{
    /**
     * @title   处理聚集查询ScopeFilter
     * @desc
     * @version
     * @author  hauser
     * @package app\Console\Commands\Generator\Lib
     * @since
     * @params  type filedName required?
     * @param array $fieldsArr
     * @return string
     * @returns
     * []
     * @returns
     */
    public function generateScopeFilter(array $fieldsArr = [])
    {
        $scopeFilterString = '';
        if ($fieldsArr) {
            $scopeFilterArr = [];
            foreach ($fieldsArr as $v) {
                if ($v != 'third_id') {
                    $scopeFilterArr[] = "
        //Search By $v
        if (isset(\$params['" . $v . "']) && \$params['" . $v . "']) {
            \$query->where('" . $v . "', '=', \$params['" . $v . "']);
        }";
                } else {
                    $scopeFilterArr[] = "
        //Search By third_id
        if (isset(\$params['third_id']) && \$params['third_id']) {
            \$thirdId = is_array(\$params['third_id']) ? \$params['third_id'] : array_map('strval', explode(',', \$params['third_id']));
            \$query->whereIn('third_id', \$thirdId);
        }";
                }

            }

            $scopeFilterString = implode("\n", $scopeFilterArr);
        }

        return $scopeFilterString;
    }

    /**
     * @title   预处理字段，增加引号
     * @desc
     * @version
     * @author  hauser
     * @package app\Console\Commands\Generator\Lib
     * @since
     * @params  type filedName required?
     * @param array $fieldsArr
     * @return string
     * @returns
     * []
     * @returns
     */
    public function preFields(array $fieldsArr = [])
    {
        $_fields = '';
        if ($fieldsArr) {
            $_fieldsArr = [];
            foreach ($fieldsArr as $v) {
                $_fieldsArr[] = "'" . $v . "'";
            }
            $_fields = implode(" ,", $_fieldsArr);
        }

        return '[' . $_fields . ']';
    }

    /**
     * @title   预处理名称
     * @desc
     * @version
     * @author  hauser
     * @package app\Console\Commands\Generator\Lib
     * @since
     * @params  type filedName required?
     * @param $table
     * @return array
     * @returns
     * []
     * @returns
     */
    public function preName($table)
    {
        if (!$table) {
            throw new \RuntimeException("table can't be null", 2);
        }

        $arr = explode('/', $table);
        $classPath = '';
        $className = '';
        $namespace = '';
        $count = count($arr);
        if ($count > 1) {
            foreach ($arr as $key => $val) {
                if ($key == $count - 1) {
                    $className .= ucfirst($this->camel_case($val));
                    $table = $val;
                } else {
                    $namespace .= "\\" . ucfirst($this->camel_case($val));
                    $classPath .= ucfirst($this->camel_case($val)) . DIRECTORY_SEPARATOR;
                }
            }
        } else {
            $className = ucfirst($this->camel_case($table));
        }

        //var_dump($table, $className);
        //die();

        return [
            /*'className' => ucfirst($this->camel_case($table)),
            'controllerName' => ucfirst($this->camel_case($table)) . 'Controller',*/
            'table' => $table,
            'className' => $className,
            'classPath' => $classPath,
            'namespace' => $namespace,
            'controllerName' => $className . 'Controller',
        ];
    }

    /**
     * @title   空格 or 下划线 or 转折线 转驼峰
     * @desc
     * @version
     * @author  hauser
     * @package Laravel\ModulesGenerator\Generator\Lib
     * @since
     * @params  type filedName required?
     * @param $str
     * @return string
     * @returns
     * ''
     * @returns
     */
    public function camel_case($str)
    {
        //处理破折号
        $arr = explode('-',$str);
        for($i=0;$i<count($arr);$i++) {
            $arr[$i] = ucfirst($arr[$i]);
        }
        $str = implode('',$arr);
        //处理空格
        $arr = explode(' ',$str);
        for($i=0;$i<count($arr);$i++) {
            $arr[$i] = ucfirst($arr[$i]);
        }
        $str = implode('',$arr);
        //处理下划线
        $arr = explode('_',$str);
        for($i=0;$i<count($arr);$i++) {
            $arr[$i] = ucfirst($arr[$i]);
        }
        $str = implode('',$arr);
        return $str;
    }
}