<?php
namespace Laravel\ModulesGenerator\Generator\Lib;

/**
 * Created by PhpStorm.
 * User: hauser
 * Date: 17-3-28
 * Time: 下午5:35
 */
class Mongo
{
    protected $table = NULL;

    protected $config;

    public function __construct($table)
    {
        $this->table = $table;

        $this->getConfig();
    }

    private function getConfig()
    {
        $configFile = dirname(__DIR__) . DIRECTORY_SEPARATOR .DIRECTORY_SEPARATOR . 'Mongo'.$this->table.'.php';
        if(!file_exists($configFile)){
            throw new \RuntimeException('config file is not exist!');
        }

        $this->config = require_once $configFile;
    }

    public function getColumns()
    {
        return isset($this->config['fields']) ? $this->config['fields'] : NULL;
    }

    /**
     * @title   Get table comment
     * @desc
     * @version
     * @author  hauser
     * @package app\Console\Commands\Generator
     * @since
     * @params  type filedName required?
     * @return null
     * @returns
     * []
     * @returns
     */
    public function getTableComment()
    {
        return isset($this->config['info']['comment']) ? $this->config['info']['comment'] : '';
    }

    public function getCasts()
    {
        return isset($this->config['casts']) ? $this->config['casts'] : '';
    }
}