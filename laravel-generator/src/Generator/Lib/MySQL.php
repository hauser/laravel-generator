<?php
namespace Laravel\ModulesGenerator\Generator\Lib;

/**
 * Created by PhpStorm.
 * User: hauser
 * Date: 17-3-28
 * Time: 下午5:35
 */
class MySQL
{
    protected $table = NULL;

    public function __construct($table)
    {
        $this->table = $table;
    }

    public function getColumns()
    {

        $fields = [];
        $columnsInfo = app('db')->connection('mysql')->select('SHOW FULL COLUMNS FROM ' . Config('database.connections.mysql.prefix') . $this->table);

        if (!$columnsInfo) {
            throw new \RuntimeException("table is not exist!", 2);
        }

        \Log::info(var_export($columnsInfo, TRUE));

        foreach ($columnsInfo as $k=>$v) {
            $fields[$v->Field] = $v->Comment;
        }

        return $fields;
    }

    /**
     * @title   Get table comment
     * @desc
     * @version
     * @author  hauser
     * @package app\Console\Commands\Generator
     * @since
     * @params  type filedName required?
     * @return null
     * @returns
     * []
     * @returns
     */
    public function getTableComment()
    {
        $tableInfo = app('db')->connection('mysql')->select("SHOW TABLE STATUS LIKE '" . Config('database.connections.mysql.prefix') . $this->table . "'");

        return isset($tableInfo[0]->Comment) && $tableInfo[0]->Comment ? $tableInfo[0]->Comment : $this->table;
    }

    public function getCasts()
    {
        return [];
    }
}