<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-4
 * Time: 下午3:08
 */

return [
    'info'   => [
        'name'    => 'Org',
        'comment' => '油品机构表'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'                  => '主键ID',
        'sys_id'              => '系统ID',
        'third_id'            => '业务系统pk',
        'parent_code'         => '父级编码',
        'orgcode'             => '机构号',
        'org_name'            => '机构名称',
        'operators_id'        => '运营商ID',
        'is_recepit_nowtime'  => '是否可以开具到当前时间的发票 1否，2是',
        'is_del'              => '是否已删除（0默认，1已删除）',
        'status'              => '状态（-1 退出 0 待用 1使用中）',
        'single_day_ceil'     => '手续费单日上限（1 所有支付方式择低 2 支付方式单独核算）',
        'is_test'             => '是否测试机构1否，2是',
        'exclusive_custom'    => '专属客服',
        'creator_name'        => '创建人名称',
        'updater_name'        => '最后修改者姓名',
        'createtime'          => '业务系统创建时间',
        'updatetime'          => '业务系统更新时间',
        'deletetime'          => '业务系统删除时间',
        'created_at'          => '创建时间',
        'updated_at'          => '更新时间',
        'deleted_at'          => '删除时间'
    ],
    'casts'  => [
        'id'                  => 'string',
        'sys_id'              => 'string',
        'third_id'            => 'string',
        'parent_code'         => 'string',
        'orgcode'             => 'string',
        'org_name'            => 'string',
        'operators_id'        => 'int',
        'is_recepit_nowtime'  => 'int',
        'is_del'              => 'int',
        'status'              => 'int',
        'single_day_ceil'     => 'int',
        'is_test'             => 'int',
        'exclusive_custom'    => 'int',
        'creator_name'        => 'string',
        'updater_name'        => 'string',
        'createtime'          => 'string',
        'updatetime'          => 'string',
        'deletetime'          => 'string',
        'created_at'          => 'string',
        'updated_at'          => 'string',
        'deleted_at'          => 'string'
    ]
];