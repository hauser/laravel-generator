<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'AccountMoneyTransfer',
        'comment' => '转账申请-现金'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'           => '主键ID',
        'sys_id'       => '系统ID',
        'third_id'     => '业务系统pk',
        'no'           => '单号',
        'no_type'      => '单号类型（单号前两位）',
        'org_id'       => '组织ID',
        'into_org_id'  => '转入机构org_id',
        'from_orgcode' => '转出机构号',
        'from_orgname' => '转出机构名称',
        'into_orgcode' => '转入机构号',
        'into_orgname' => '转入机构名称',
        'app_time'     => '申请时间',
        'money'        => '转账金额',
        'use_fanli'    => '此次转账使用返利金额',
        'status'       => '状态（-1已驳回，0待审核，1已审核,-2已删除）',
        '_status'      => '状态名称',
        'data_from'    => '数据来源（1gsp，2web，3app）',
        'remark'       => '备注',
        'remark_work'  => '备注/外',
        'creator_name' => '创建人',
        'updater_name' => '更新人',
        'createtime'   => '业务系统创建时间',
        'updatetime'   => '业务系统更新时间',
        'deletetime'   => '业务系统删除时间',
        'created_at'   => '创建时间',
        'updated_at'   => '更新时间',
        'deleted_at'   => '删除时间'
    ],
    'casts'  => [
        'id'           => 'string',
        'third_id'     => 'string',
        'sys_id'       => 'string',
        'no'           => 'string',
        'no_type'      => 'string',
        'org_id'       => 'string',
        'into_org_id'  => 'string',
        'from_orgcode' => 'string',
        'from_orgname' => 'string',
        'into_orgcode' => 'string',
        'into_orgname' => 'string',
        'app_time'     => 'string',
        'money'        => 'string',
        'use_fanli'    => 'string',
        'status'       => 'string',
        '_status'      => 'string',
        'data_from'    => 'string',
        'creator_name' => 'string',
        'updater_name' => 'string',
        'remark'       => 'string',
        'remark_work'  => 'string',
        'createtime'   => 'string',
        'updatetime'   => 'string',
        'created_at'   => 'string',
        'updated_at'   => 'string'
    ]
];