<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-12
 * Time: 下午5:14
 */

return [
    'info'   => [
        'name'    => 'ReceiptTitle',
        'comment' => '发票抬头维护'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'               => '主键ID',
        'sys_id'           => '系统ID',
        'third_id'         => '业务系统pk',
        'org_id'           => '机构ID',
        'org_code'         => '机构号',
        'org_name'         => '机构名称',
        'corp_name'        => '企业名称(发票抬头)',
        'receipt_type'     => '开票类型(0:专票,1:普票)',
        'corp_addr'        => '公司地址',
        'corp_tel'         => '公司电话',
        'taxpayer_no'      => '纳税人识别号',
        'bank_name'        => '开户行',
        'bank_account'     => '开户行账号',
        'status'           => '状态（0待审核，1已审核,-1已驳回）',
        'admin_remark'     => '后台备注',
        'custom_remark'    => '客户备注',
        'corp_licence'     => '单位营业执照',
        'taxpayer_licence' => '一般纳税人资格证',
        'tax_licence'      => '税务登记证',
        'bank_licence'     => '银行开户许可证',
        'is_previous'      => '是否上次使用(0:不是1:是)',
        'is_del'           => '删除(0:未删除1:已删除)',
        'other_creator_id' => '接口添加人id',
        'other_creator'    => '接口添加人名称',
        'last_operator'    => '最后操作人',
        'creator_id'       => '创建人ID',
        'creator_name'     => '创建人名称',
        'updater_id'       => '最后修改者姓名',
        'updater_name'     => '最后修改者姓名',
        'createtime'       => '业务系统创建时间',
        'updatetime'       => '业务系统更新时间',
        'deletetime'       => '业务系统删除时间',
        'created_at'       => '创建时间',
        'updated_at'       => '更新时间',
        'deleted_at'       => '删除时间'
    ],
    'casts'  => [
        'id'               => 'string',
        'sys_id'           => 'string',
        'third_id'         => 'string',
        'org_id'           => 'string',
        'org_code'         => 'string',
        'org_name'         => 'string',
        'corp_name'        => 'string',
        'receipt_type'     => 'string',
        'corp_addr'        => 'string',
        'corp_tel'         => 'string',
        'taxpayer_no'      => 'string',
        'bank_name'        => 'string',
        'bank_account'     => 'string',
        'status'           => 'int',
        'admin_remark'     => 'string',
        'custom_remark'    => 'string',
        'corp_licence'     => 'string',
        'taxpayer_licence' => 'string',
        'tax_licence'      => 'string',
        'bank_licence'     => 'string',
        'is_previous'      => 'int',
        'is_del'           => 'int',
        'other_creator_id' => 'string',
        'other_creator'    => 'string',
        'last_operator'    => 'string',
        'creator_id'       => 'string',
        'creator_name'     => 'string',
        'updater_id'       => 'string',
        'updater_name'     => 'string',
        'createtime'       => 'string',
        'updatetime'       => 'string',
        'deletetime'       => 'string',
        'created_at'       => 'string',
        'updated_at'       => 'string',
        'deleted_at'       => 'string'
    ]
];