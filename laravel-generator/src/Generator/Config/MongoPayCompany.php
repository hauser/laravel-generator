<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-15
 * Time: 下午5:18
 */
return [
    'info'   => [
        'name'    => 'PayCompany',
        'comment' => '机构付款公司表'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'               => '主键ID',
        'sys_id'           => '系统ID',
        'third_id'         => '业务系统pk',
        'orgroot'          => '机构编码',
        'org_name'         => '机构名称',
        'company_name'     => '付款公司名称',
        'status'           => '状态 1，正常，2停用',
        'remark'           => '备注',
        'creator'          => '创建人姓名',
        'last_operator_id' => '最后修改人id',
        'last_operator'    => '最后修改人姓名',
        'creator_id'       => '创建人ID',
        'creator_name'     => '创建人名称',
        'updater_id'       => '最后修改者姓名',
        'updater_name'     => '最后修改者姓名',
        'createtime'       => '业务系统创建时间',
        'updatetime'       => '业务系统更新时间',
        'deletetime'       => '业务系统删除时间',
        'created_at'       => '创建时间',
        'updated_at'       => '更新时间',
        'deleted_at'       => '删除时间'
    ],
    'casts'  => [
        'id'               => 'string',
        'sys_id'           => 'string',
        'third_id'         => 'string',
        'orgroot'          => 'string',
        'org_name'         => 'string',
        'company_name'     => 'string',
        'status'           => 'int',
        'remark'           => 'string',
        'creator'          => 'string',
        'last_operator_id' => 'string',
        'last_operator'    => 'string',
        'creator_id'       => 'string',
        'creator_name'     => 'string',
        'updater_id'       => 'string',
        'updater_name'     => 'string',
        'createtime'       => 'string',
        'updatetime'       => 'string',
        'deletetime'       => 'string',
        'created_at'       => 'string',
        'updated_at'       => 'string',
        'deleted_at'       => 'string'
    ]
];