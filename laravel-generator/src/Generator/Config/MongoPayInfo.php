<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-15
 * Time: 上午10:27
 */

return [
    'info'   => [
        'name'    => 'PayInfo',
        'comment' => '机构付款信息表'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'                => '主键ID',
        'sys_id'            => '系统ID',
        'third_id'          => '业务系统pk',
        'orgroot'           => '顶级机构编码',
        'org_name'          => '顶级机构名称',
        'account_no'        => '账号',
        'pay_type'          => '支付方式：1 对公转账、2 POS转账 - 借记卡、3 POS转账 - 贷记卡、4 微信支付、5 其他',
        'pay_channel'       => '渠道',
        'operators_id'      => '运营商id',
        'operators_name'    => '运营商名称',
        'share_fee_type'    => '手续费分摊方式（1，百分比；2，收款方封顶；3，付款方封顶）',
        'share_start_money' => '分摊起始额',
        'payer_fee_ratio'   => '付款方手续费',
        'payee_fee_ratio'   => '收款方手续费',
        'single_time_limit' => '手续费单次限额',
        'single_day_time'   => '单日限次',
        'single_day_limit'  => '手续费单日限额',
        'status'            => '状态 1，正常，2停用',
        'remark'            => '备注',
        'creator'           => '创建人姓名',
        'last_operator_id'  => '最后修改人id',
        'last_operator'     => '最后修改人姓名',
        'creator_id'        => '创建人ID',
        'creator_name'      => '创建人名称',
        'updater_id'        => '最后修改者姓名',
        'updater_name'      => '最后修改者姓名',
        'createtime'        => '业务系统创建时间',
        'updatetime'        => '业务系统更新时间',
        'deletetime'        => '业务系统删除时间',
        'created_at'        => '创建时间',
        'updated_at'        => '更新时间',
        'deleted_at'        => '删除时间'
    ],
    'casts'  => [
        'id'                => 'string',
        'sys_id'            => 'string',
        'third_id'          => 'string',
        'orgroot'           => 'string',
        'org_name'          => 'string',
        'account_no'        => 'string',
        'pay_type'          => 'int',
        'pay_channel'       => 'string',
        'operators_id'      => 'string',
        'operators_name'    => 'string',
        'share_fee_type'    => 'int',
        'share_start_money' => 'double',
        'payer_fee_ratio'   => 'double',
        'payee_fee_ratio'   => 'double',
        'single_time_limit' => 'double',
        'single_day_time'   => 'int',
        'single_day_limit'  => 'double',
        'status'            => 'int',
        'remark'            => 'string',
        'creator'           => 'string',
        'last_operator_id'  => 'string',
        'last_operator'     => 'string',
        'creator_id'        => 'string',
        'creator_name'      => 'string',
        'updater_id'        => 'string',
        'updater_name'      => 'string',
        'createtime'        => 'string',
        'updatetime'        => 'string',
        'deletetime'        => 'string',
        'created_at'        => 'string',
        'updated_at'        => 'string',
        'deleted_at'        => 'string'
    ]
];