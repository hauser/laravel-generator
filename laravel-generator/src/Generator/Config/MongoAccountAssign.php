<?php
/**
 * Created by PhpStorm.
 * User: Hauser
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'AccountAssign',
        'comment' => '分配申请单'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'               => 'id',
        'sys_id'           => 'sys_id',
        'third_id'         => '业务系统ID',
        'no'               => '单号',
        'no_type'          => '单号类型（单号前两位）',
        'assign_type'      => '10自动分配，20非自动分配，30撬装分配，40积分分配',
        'org_id'           => '组织ID',
        'account_type'     => '10 资金账户 20 授信账户,30 撬装账户 40 托管卡账户',
        'account_no'       => '扣款账户',
        'money_total'      => '分配总金额',
        'use_cash_fanli'   => '使用现金返利',
        'jifen_total'      => '分配总积分',
        'assign_num'       => '分配卡数',
        'charge_id'        => '充值id',
        'status'           => '状态（-1已驳回，0待审核，1已审核）',
        'check_assign'     => '分配校验(0:未校验,-10:校验失败,5:分配校验中,10:校验成功)',
        'data_from'        => '数据来源（1.gsp，2.web，3.app 4,webchat）',
        'provider_flag'    => '1，中石油中石化 2，撬装 0 其他',
        'remark'           => '备注',
        'remark_work'      => '备注/外',
        'apply_time'       => '申请时间',
        'creator_name'       => '创建人',
        'last_operator'    => '最后修改者姓名',
        'other_creator'    => '接口添加人名称',
        'complete_person'  => '完成人的名称',
        'complete_time'    => '完成时间',
        'unit'             => '分配类型1现金2油量',
        'createtime'       => '业务系统创建时间',
        'updatetime'       => '业务系统更新时间',
        'deletetime'       => '业务系统删除时间',
        'created_at'       => '创建时间',
        'updated_at'       => '更新时间',
        'deleted_at'       => '删除时间'
    ],
    'casts'  => [
        'id'               => 'string',
        'sys_id'           => 'string',
        'third_id'         => 'string',
        'no'               => 'string',
        'no_type'          => 'string',
        'assign_type'      => 'string',
        'org_id'           => 'string',
        'account_type'     => 'string',
        'account_no'       => 'string',
        'money_total'      => 'double',
        'use_cash_fanli'   => 'double',
        'jifen_total'      => 'double',
        'assign_num'       => 'string',
        'charge_id'        => 'string',
        'status'           => 'string',
        'check_assign'     => 'string',
        'data_from'        => 'string',
        'provider_flag'    => 'string',
        'remark'           => 'string',
        'remark_work'      => 'string',
        'apply_time'       => 'string',
        'creator_name'     => 'string',
        'last_operator'    => 'string',
        'other_creator'    => 'string',
        'complete_person'  => 'string',
        'complete_time'    => 'string',
        'unit'             => 'int',
        'createtime'       => 'string',
        'updatetime'       => 'string',
        'deletetime'       => 'string',
        'created_at'       => 'string',
        'updated_at'       => 'string',
        'deleted_at'       => 'string'
    ]
];