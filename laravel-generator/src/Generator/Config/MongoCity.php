<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-4
 * Time: 下午3:08
 */

return [
    'info'   => [
        'name'    => 'City',
        'comment' => '省市表'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'         => '省市Code',
        'code'       => '省市Code',
        'name'       => '省市名',
        'address'    => '完整地址',
        'level'      => '等级,省 :2;市:3;区/县:4',
        'parent'     => '父级code',
        'created_at' => '创建时间',
        'updated_at' => '更新时间',
    ],
    'casts'  => [
        'id'         => 'string',
        'code'       => 'string',
        'name'       => 'string',
        'address'    => 'string',
        'level'      => 'integer',
        'parent'     => 'string',
        'created_at' => 'string',
        'updated_at' => 'string',
    ]
];