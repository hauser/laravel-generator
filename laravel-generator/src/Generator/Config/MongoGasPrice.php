<?php

return [
    'info'   => [
        'name'    => 'GasPrice',
        'comment' => '撬装卡油价'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'           => '主键ID',
        'sys_id'       => '系统ID',
        'third_id'     => '业务系统pk',
        'price'        => '批发吨位价',
        'litre_price'  => '批发升位价',
        'market_price' => '市场挂牌吨位价',
        'min'          => '当日最低价',
        'max'          => '当日最高价',
        'avg_seven'    => '标签名称',
        'createtime'   => '业务系统创建时间',
        'created_at'   => '创建时间',
        'updated_at'   => '更新时间',
        'deleted_at'   => '删除时间'
    ],
    'casts'  => [
        'id'           => 'string',
        'third_id'     => 'string',
        'sys_id'       => 'string',
        'price'        => 'double',
        'litre_price'  => 'double',
        'market_price' => 'double',
        'min'          => 'double',
        'max'          => 'double',
        'avg_seven'    => 'double',
        'createtime'   => 'string',
        'created_at'   => 'string',
        'updated_at'   => 'string'
    ]
];