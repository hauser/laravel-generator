<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'RequestLog',
        'comment' => '请求日志'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'         => 'id',
        'path'       => '请求路径',
        'params'     => '参数',
        'app_id'     => '请求应用ID',
        'sys_id'     => '业务系统ID',
        'method'     => 'post/get',
        'data'       => '返回值',
        'mem_use'    => '内存使用',
        'time_use'   => '内存使用',
        'ip'         => '请求方IP',
        'status'     => '状态',
        'created_at' => '创建时间',
        'updated_at' => '更新时间',
        'deleted_at' => '删除时间'
    ],
    'casts'  => [
        'id'         => 'string',
        'path'       => 'string',
        'params'     => 'json',
        'app_id'     => 'string',
        'sys_id'     => 'string',
        'method'     => 'string',
        'data'       => 'string',
        'mem_use'    => 'float',
        'time_use'   => 'float',
        'ip'         => 'string',
        'status'     => 'integer',
        'created_at' => 'string',
        'updated_at' => 'string',
        'deleted_at' => 'string'
    ]
];