<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-15
 * Time: 下午5:26
 */

return [
    'info'   => [
        'name'    => 'CreditAccount',
        'comment' => '授信账户'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'                   => '主键ID',
        'sys_id'               => '系统ID',
        'third_id'             => '业务系统pk',
        'account_no'           => '授信账号',
        'org_id'               => '机构编码',
        'org_name'             => '机构名称',
        'operators_id'         => '运营商ID',
        'operators_name'       => '运营商名称',
        'credit_provider_id'   => '授信提供方id',
        'credit_provider_name' => '授信提供方名称',
        'credit_total'         => '授信总额',
        'used_total'           => '已用额度',
        'credit_blance'        => '可用金额（含冻结）',
        'assign_total'         => '累计分配',
        'repay_total'          => '累计还款',
        'last_assign_time'     => '最新分配时间',
        'last_repay_time'      => '最新还款时间',
        'remark'               => '内部备注',
        'remark_work'          => '外部备注',
        'status'               => '状态10正常，20停用',
        'creator_name'         => '创建人名称',
        'updater_name'         => '最后修改者姓名',
        'createtime'           => '业务系统创建时间',
        'updatetime'           => '业务系统更新时间',
        'deletetime'           => '业务系统删除时间',
        'created_at'           => '创建时间',
        'updated_at'           => '更新时间',
        'deleted_at'           => '删除时间'
    ],
    'casts'  => [
        'id'                   => 'string',
        'sys_id'               => 'string',
        'third_id'             => 'string',
        'account_no'           => 'string',
        'org_id'               => 'string',
        'org_name'             => 'string',
        'operators_id'         => 'string',
        'operators_name'       => 'string',
        'credit_provider_id'   => 'string',
        'credit_provider_name' => 'string',
        'credit_total'         => 'double',
        'used_total'           => 'double',
        'credit_blance'        => 'double',
        'assign_total'         => 'double',
        'repay_total'          => 'double',
        'last_assign_time'     => 'string',
        'last_repay_time'      => 'string',
        'remark'               => 'string',
        'remark_work'          => 'string',
        'status'               => 'int',
        'creator_name'         => 'string',
        'updater_name'         => 'string',
        'createtime'           => 'string',
        'updatetime'           => 'string',
        'deletetime'           => 'string',
        'created_at'           => 'string',
        'updated_at'           => 'string',
        'deleted_at'           => 'string'
    ]
];