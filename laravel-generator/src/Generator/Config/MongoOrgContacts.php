<?php
/**
 * created by phpstorm.
 * user: love
 * date: 17-5-12
 * time: 上午11:52
 */


return [
    'info'   => [
        'name'    => 'OrgContacts',
        'comment' => '油品机构联系人'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'               => '主键id',
        'sys_id'           => '系统id',
        'third_id'         => '业务系统pk',
        'org_id'           => '机构id',
        'org_name'         => '机构名称',
        'contact_name'     => '机构负责人',
        'contact_mobile'   => '机构负责人电话',
        'contact_mobile2'  => '油品负责人联系电话2',
        'contact_email'    => '机构负责人邮箱',
        'contact_region'   => '油品负责人地区',
        'contact_qq'       => '油品负责人qq号',
        'remark'           => '机构负责人备注',
        'is_active'        => '是否有效（1是，0否）',
        'is_del'           => '状态：1删除 0正常',
        'creator_name'     => '创建人名称',
        'updater_name'     => '最后修改者姓名',
        'createtime'       => '业务系统创建时间',
        'updatetime'       => '业务系统更新时间',
        'deletetime'       => '业务系统删除时间',
        'created_at'       => '创建时间',
        'updated_at'       => '更新时间',
        'deleted_at'       => '删除时间'
    ],
    'casts'  => [
        'id'               => 'string',
        'sys_id'           => 'string',
        'third_id'         => 'string',
        'org_id'           => 'string',
        'org_name'         => 'string',
        'contact_name'     => 'string',
        'contact_mobile'   => 'string',
        'contact_mobile2'  => 'string',
        'contact_email'    => 'string',
        'contact_region'   => 'string',
        'contact_qq'       => 'string',
        'remark'           => 'string',
        'is_active'        => 'int',
        'is_del'           => 'int',
        'creator_name'     => 'string',
        'updater_name'     => 'string',
        'createtime'       => 'string',
        'updatetime'       => 'string',
        'deletetime'       => 'string',
        'created_at'       => 'string',
        'updated_at'       => 'string',
        'deleted_at'       => 'string'
    ]
];