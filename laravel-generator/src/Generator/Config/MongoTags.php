<?php

return [
    'info'   => [
        'name'    => 'Tags',
        'comment' => '标签表'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'           => '主键ID',
        'sys_id'       => '系统ID',
        'third_id'     => '业务系统pk',
        'orgcode'      => '副卡ID',
        'role_id'      => '角色ID',
        'user_id'      => '用户ID',
        'tag_name'     => '标签名称',
        'creator_name' => '创建人',
        'updater_name' => '更新人',
        'createtime'   => '业务系统创建时间',
        'updatetime'   => '业务系统更新时间',
        'deletetime'   => '业务系统删除时间',
        'created_at'   => '创建时间',
        'updated_at'   => '更新时间',
        'deleted_at'   => '删除时间'
    ],
    'casts'  => [
        'id'           => 'string',
        'third_id'     => 'string',
        'sys_id'       => 'string',
        'orgcode'      => 'string',
        'role_id'      => 'string',
        'user_id'      => 'string',
        'tag_name'     => 'string',
        'creator_name' => 'string',
        'updater_name' => 'string',
        'createtime'   => 'string',
        'updatetime'   => 'string',
        'created_at'   => 'string',
        'updated_at'   => 'string'
    ]
];