<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'ViceTags',
        'comment' => '卡标签关系表'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'         => '主键ID',
        'sys_id'     => '系统ID',
        'third_id'   => '业务系统pk',
        'vice_id'    => '副卡ID',
        'tag_id'     => '标签ID',
        'createtime' => '业务系统创建时间',
        'updatetime' => '业务系统更新时间',
        'deletetime' => '业务系统删除时间',
        'created_at' => '创建时间',
        'updated_at' => '更新时间',
        'deleted_at' => '删除时间'
    ],
    'casts'  => [
        'id'         => 'string',
        'third_id'   => 'string',
        'sys_id'     => 'string',
        'vice_id'    => 'string',
        'tag_id'     => 'string',
        'createtime' => 'string',
        'updatetime' => 'string',
        'created_at' => 'string',
        'updated_at' => 'string'
    ]
];