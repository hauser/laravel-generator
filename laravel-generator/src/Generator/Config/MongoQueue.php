<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'Queue',
        'comment' => '队列信息'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'           => 'id',
        'task_id'      => '任务id',
        'queue_driver' => '队列驱动',
        'app_id'       => '请求应用ID',
        'sys_id'       => '业务系统ID',
        'action'       => '数据操作行为',
        'method'       => '请求方法',
        'data_type'    => '数据类型',
        'data'         => '参数',
        'closure'      => '队列执行逻辑',
        'ip'           => '请求方IP',
        'callback_url' => '回调地址',
        'status'       => '状态',
        'created_at'   => '创建时间',
        'updated_at'   => '更新时间',
        'deleted_at'   => '删除时间'
    ],
    'casts'  => [
        'id'           => 'string',
        'task_id'      => 'string',
        'queue_driver' => 'string',
        'app_id'       => 'string',
        'sys_id'       => 'string',
        'action'       => 'string',
        'method'       => 'string',
        'data_type'    => 'string',
        'ip'           => 'string',
        'callback_url' => 'string',
        'status'       => 'integer',
        'created_at'   => 'string',
        'updated_at'   => 'string',
        'deleted_at'   => 'string'
    ]
];