<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'CardRegister',
        'comment' => '用卡登记'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'           => 'id',
        'sys_id'       => 'sys_id',
        'third_id'     => '业务系统ID',
        'vice_no'      => '卡号',
        'truck_no'     => '车牌号',
        'status'       => '0，代表未登记，1代表发放，2代表收回，3代表补登',
        'sendtime'     => '发放时间',
        'backtime'     => '收回时间',
        'remark'       => '油卡备注',
        'creator_name' => '创建人',
        'updater_name' => '修改人',
        'createtime'   => '业务系统创建时间',
        'updatetime'   => '业务系统更新时间',
        'deletetime'   => '业务系统删除时间',
        'created_at'   => '创建时间',
        'updated_at'   => '更新时间',
        'deleted_at'   => '删除时间'
    ],
    'casts'  => [
        'id'           => 'string',
        'sys_id'       => 'string',
        'third_id'     => 'string',
        'vice_no'      => 'string',
        'truck_no'     => 'string',
        'status'       => 'string',
        'sendtime'     => 'string',
        'backtime'     => 'string',
        'remark'       => 'string',
        'creator_name' => 'string',
        'updater_name' => 'string',
        'createtime'   => 'string',
        'updatetime'   => 'string',
        'deletetime'   => 'string',
        'created_at'   => 'string',
        'updated_at'   => 'string',
        'deleted_at'   => 'string'
    ]
];