<?php
/**
 * Created by PhpStorm.
 * User: kevin
 * Date: 17-3-28
 * Time: 下午5:18
 */

return [
    'info'   => [
        'name'    => 'CardMain',
        'comment' => '主卡信息'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'               => 'id',
        'sys_id'           => 'sys_id',
        'third_id'         => '业务系统ID',
        'main_no'          => '主卡号',
        'main_jifen_id'    => '返利积分主卡id',
        'main_jifen_no'    => '天津返利积分卡卡号',
        'oil_com'          => '油卡商（油卡类型）（1、中石化，2、中石油）',
        'active_region'    => '发卡省份（关联省份表）',
        'fanli_region'     => '积分可用地区（关联省份表）',
        'card_owner'       => '持卡人',
        'operators_id'     => '运营商id',
        'active_time'      => 'active_time',
        'account_remain'   => '账户余额',
        'point_remain'     => '积分余额',
        'remain_syn_time'  => '余额同步时间',
        'account_name'     => '账户名称 ',
        'account_password' => '账户密码',
        'remark'           => '备注',
        'refresh_status'   => '余额实时刷新状态（-10刷新失败，10刷新成功,5刷新中）',
        'is_jifen'         => '0是普通主卡，1是积分主卡',
        'creator_id'       => '创建人ID（操作员）',
        'last_operator'    => '最后修改者姓名',
        'creator_name'     => '创建人名称',
        'updater_id'       => '最后修改者姓名',
        'updater_name'     => '最后修改者姓名',
        'trade_time'       => '最新消费时间',
        'createtime'       => '业务系统创建时间',
        'updatetime'       => '业务系统更新时间',
        'deletetime'       => '业务系统删除时间',
        'created_at'       => '创建时间',
        'updated_at'       => '更新时间',
        'deleted_at'       => '删除时间'
    ]
];