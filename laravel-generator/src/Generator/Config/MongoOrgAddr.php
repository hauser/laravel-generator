<?php
/**
 * Created by PhpStorm.
 * User: love
 * Date: 17-5-11
 * Time: 下午3:08
 */

return [
    'info'   => [
        'name'    => 'OrgAddr',
        'comment' => '油品机构收卡地址'
    ],
    'type'   => 'mongo',
    'fields' => [
        'id'                  => '主键ID',
        'sys_id'              => '系统ID',
        'third_id'            => '业务系统pk',

        'org_id'         => '所属机构ID',
        'org_name'         => '所属机构名称',
        'name'             => '姓名',
        'mobile'            => '电话',
        'province_id'        => '所属省id (code)',
        'province_name'  => '所属省name',
        'city_id'              => '所属市id(code)',
        'city_name'              => '所属市name',
        'district_id'     => '所属区县id(code)',
        'district_name'             => '所属区县name',
        'address'    => '地址',
        'addr_zip'    => '邮编',
        'remark'    => '备注',
        'last_operator'    => '最后修改者姓名',
        'other_operator_id'    => '更新人id',
        'other_operator'    => '更新人名称',
        'is_del'    => '状态：1删除 0正常',
        'other_creator_id'    => '接口添加人id',
        'other_creator'    => '接口添加人名称',
        'card_default'    => '默认油卡邮寄地址：0不默认，1为默认',
        'invoice_default'    => '默认发票邮寄地址：0不默认，1为默认',

        'creator_id'          => '创建人ID',
        'creator_name'        => '创建人名称',
        'updater_id'          => '最后修改者姓名',
        'updater_name'        => '最后修改者姓名',
        'createtime'          => '业务系统创建时间',
        'updatetime'          => '业务系统更新时间',
        'deletetime'          => '业务系统删除时间',
        'created_at'          => '创建时间',
        'updated_at'          => '更新时间',
        'deleted_at'          => '删除时间'
    ],
    'casts'  => [
        'id'                  => 'string',
        'sys_id'              => 'string',
        'third_id'            => 'string',

        'org_id'         => 'string',
        'org_name'         => 'string',
        'name'             => 'string',
        'mobile'            => 'string',
        'province_id'        => 'string',
        'province_name'  => 'string',
        'city_id'              => 'string',
        'city_name'              => 'string',
        'district_id'     => 'string',
        'district_name'             => 'string',
        'address'    => 'string',
        'addr_zip'    => 'string',
        'remark'    => 'string',
        'last_operator'    => 'string',
        'other_operator_id'    => 'string',
        'other_operator'    => 'string',
        'is_del'    => 'int',
        'other_creator_id'    => 'string',
        'other_creator'    => 'string',
        'card_default'    => 'string',
        'invoice_default'    => 'string',

        'creator_id'          => 'string',
        'creator_name'        => 'string',
        'updater_id'          => 'string',
        'updater_name'        => 'string',
        'createtime'          => 'string',
        'updatetime'          => 'string',
        'deletetime'          => 'string',
        'created_at'          => 'string',
        'updated_at'          => 'string',
        'deleted_at'          => 'string'
    ]
];